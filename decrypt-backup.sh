#!/bin/bash
# written by brandonia

# A script to decrypt a device designated as a backup.

color='\e[0;34m'
reset='\e[0m'

# welcome text
echo -e "${color}--- Welcome to decrypt-backup.sh ---${reset}"

# list out devices
lsblk -af
echo ""

# prompt for device choice
echo -e "${color}--- Choose which device to decrypt: ---${reset}"
printf "> "
read -r
device=$REPLY

# open device
sudo cryptsetup open --type luks $device backup
echo -e "${color}--- Device Opened ---${reset}"

# mount device filesystem
echo -e "${color}--- Mounting Filesystem ---${reset}"
sudo mount /dev/mapper/backup /mnt/backup

echo -e "${color}--- Enjoy! ---${reset}"