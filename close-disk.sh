#!/bin/bash

color='\e[0;34m'
reset='\e[0m'

echo -e "${color}--- Unnmounting Disk ---${reset}"
sudo umount /mnt/backup

echo -e "${color}--- Closing Device ---${reset}"
sudo cryptsetup close backup