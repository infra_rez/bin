#!/bin/bash
# written by brandonia

# A script to sync my Notes folder with my backup device

color='\e[0;34m'
reset='\e[0m'

welcome
echo -e "${color}--- Welcome to note-sync.sh ---${reset}"

echo -e "${color}--- Syncing Notes folder to backup ---${reset}"
rsync -avz /home/brandonia/infra_rez/Notes /mnt/backup/Projects/Notes

echo -e "${color}--- Notes are backed up! ---${reset}"
