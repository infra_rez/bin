#!/bin/bash

date=$(date)

# welcome text
echo "--- Welcome to this humble Backup Script"

# list out devices
lsblk -af
echo ""

# prompt for device choice
echo "--- Choose which device to use: ---"
read -r
device=$REPLY

# open device
sudo cryptsetup open --type luks $device offsite
echo "--- Device Opened ---"

# mount device filesystem
echo "--- Mounting Filesystem ---"
sudo mount /dev/mapper/offsite /mnt/temp

# rysnc backup
echo "--- Starting Backup ---"

echo "--- Pictures ---"
rsync -avz /mnt/nas/Pictures /mnt/temp
echo "--- Projects ---"
rsync -avz /mnt/nas/Projects /mnt/temp
echo "--- Documents ---"
rsync -avz /mnt/nas/Documents /mnt/temp 
echo "--- Consulting ---"
rsync -avz /mnt/nas/Consulting /mnt/temp
echo "--- Games ---"
rsync -avz /mnt/nas/Games /mnt/temp

echo "--- Backup Complete! ---"

# unmount device filesystem
echo "--- Unmounting Filesystem ---"
sudo umount /mnt/temp

# close device
echo "--- Closing Device ---"
sudo cryptsetup close offsite
